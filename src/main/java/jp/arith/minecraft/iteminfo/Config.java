package jp.arith.minecraft.iteminfo;

import com.mojang.realmsclient.gui.ChatFormatting;
import jp.arith.minecraft.iteminfo.tooltip.Tooltips;
import jp.arith.minecraft.iteminfo.tooltip.Visibility;
import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class Config {
    final Configuration cfg;

    String[] toolListCache;
    public String[] getToolList() {
        return toolListCache = toolListCache != null ? toolListCache : cfg.get("common", "tools", new String[] { "pickaxe", "axe", "shovel" }).getStringList();
    }

    void reload() {
        cfg.load();

        toolListCache = null;

        getToolList();
        tooltip.reload(this);
    }

    void save() {
        cfg.save();
    }

    final Path path;
    final WatchService watcher;
    final WatchKey watchKey;

    public final Tooltip tooltip;

    public Config(File file) {
        ItemInfoMod.LOG.info("Config file: " + file.getPath());

        this.cfg = new Configuration(file);

        this.tooltip = new Tooltip(cfg);

        reload();

        save();

        //  コンフィグファイル監視
        this.path = file.toPath();
        Path dirPath = path.getParent();
        FileSystem fs = path.getFileSystem();

        WatchService watcher = null;
        WatchKey watchKey = null;

        try {
            watcher = fs.newWatchService();

            watchKey = dirPath.register(watcher,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_MODIFY,
                    StandardWatchEventKinds.ENTRY_DELETE);


        }catch(IOException e) {
            ItemInfoMod.LOG.error("Failed starting FileWatcher.", e);
        }

        this.watcher = watcher;
        this.watchKey = watchKey;

        Runtime.getRuntime().addShutdownHook(new Shutdown());
    }

    class Shutdown extends Thread {
        @Override
        public void run() {
            try {
                if(watcher != null)
                    watcher.close();
            }catch(IOException e) {
                e.printStackTrace(System.err);
            }
            ItemInfoMod.LOG.info("FileWatcher was been closed.");
        }
    }

    public boolean pollUpdate() {
        WatchKey detectedKey = watcher.poll();
        if(detectedKey != null) {
            try {
                if (detectedKey.equals(watchKey)) {
                    ItemInfoMod.LOG.info("Detected config directory updating.");
                    for (WatchEvent<?> e : detectedKey.pollEvents()) {
                        if (e.kind() == StandardWatchEventKinds.OVERFLOW)
                            continue;

                        if (e.context() instanceof Path && path.equals(path.getParent().resolve((Path) e.context()))) {
                            ItemInfoMod.LOG.info("Detected config file updating.");
                            reload();
                            ItemInfoMod.LOG.info("Reloading config file has been completed.");
                            return true;
                        }
                    }
                }
            }finally {
                detectedKey.reset();
            }
        }

        return false;
    }


    public class Tooltip {
        final Configuration cfg;

        public Tooltip(Configuration cfg) {
            this.cfg = cfg;
        }

        public void reload(Config config) {
            for(Tooltips tt : Tooltips.values())
                tt.tooltip.reloadConfig(config);
        }

        public Visibility visibility(String name, Visibility def) {
            try {
                return Visibility.fromConfigValue(cfg.get("tooltip", name, def.getConfigValue()).getString());
            }catch(IllegalArgumentException e) {
                ItemInfoMod.LOG.error("Config file error", e);
                return Visibility.HIDDEN;
            }
        }

        public String color(String name, String def) {
            StringBuilder sb = new StringBuilder();
            for (String colName : cfg.get("tooltip", name + "Color", def).getString().split(","))
                if (!colName.isEmpty())
                    sb.append(ChatFormatting.getByName(colName));
            return new String(sb);
        }
    }




}
