package jp.arith.minecraft.iteminfo.tooltip;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class BlockHarvestLevelTooltip extends Tooltip {
    public BlockHarvestLevelTooltip() { super("blockHarvestLevel"); }

    @Override
    public String getDefaultColor() { return "BLUE"; }

    @Override
    public String getLine(ItemStack itemStack) {
        if(itemStack.getItem() instanceof ItemBlock) {
            Block block = ((ItemBlock) itemStack.getItem()).field_150939_a;
            int meta = itemStack.getItemDamage();
            if (block.getHarvestLevel(meta) >= 0)
                return block.getHarvestTool(meta) + "(" + block.getHarvestLevel(meta) + ")";
        }
        return "";
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.ADVANCED; }
}
