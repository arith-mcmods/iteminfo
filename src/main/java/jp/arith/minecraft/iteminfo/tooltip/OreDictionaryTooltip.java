package jp.arith.minecraft.iteminfo.tooltip;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class OreDictionaryTooltip extends Tooltip {
    public OreDictionaryTooltip() { super("oreDictionary"); }

    @Override
    public String getDefaultColor() { return "DARK_AQUA"; }

    @Override
    public String getLine(ItemStack itemStack) {
        int[] oreIds = OreDictionary.getOreIDs(itemStack);
        String[] oreNames = new String[oreIds.length];
        for (int i = 0; i < oreIds.length; i++)
            oreNames[i] = OreDictionary.getOreName(oreIds[i]);

        return joinStrings(", ", oreNames);
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.ADVANCED; }
}
