package jp.arith.minecraft.iteminfo.tooltip;

import jp.arith.minecraft.iteminfo.Config;
import jp.arith.minecraft.iteminfo.ItemInfoMod;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;

public abstract class Tooltip {
    public final String name;
    public Tooltip(String name) {
        this.name = name;
    }

    public String getDefaultColor() { return ""; }
    public abstract Visibility getDefaultVisibility();

    public Visibility getVisibility() { return ItemInfoMod.getConfig().tooltip.visibility(name, getDefaultVisibility()); }
    public String getLine(ItemStack itemStack) { return ""; }

    public void appendTooltip(ItemTooltipEvent ev) {
        String s = getLine(ev.itemStack);
        if(s == null || s.isEmpty()) return;

        Visibility v = getVisibility();

        if((!ev.showAdvancedItemTooltips && v.isVisibleInNotAdvanced) || (ev.showAdvancedItemTooltips && v.isVisibleInAdvanced))
            ev.toolTip.add(ItemInfoMod.getConfig().tooltip.color(name, getDefaultColor()) + s);
    }

    public void reloadConfig(Config config) {
        config.tooltip.visibility(name, getDefaultVisibility());
        config.tooltip.color(name, getDefaultColor());
    }

    protected static String joinStrings(String delim, String[] items) {
        boolean head = true;
        StringBuilder sb = new StringBuilder();
        for(String item : items) {
            if(!head)
                sb.append(delim);
            sb.append(item);
            head = false;
        }
        return new String(sb);
    }

    protected static String joinStrings(String delim, Iterable<String> items) {
        boolean head = true;
        StringBuilder sb = new StringBuilder();
        for(String item : items) {
            if(!head)
                sb.append(delim);
            sb.append(item);
            head = false;
        }
        return new String(sb);
    }

}
