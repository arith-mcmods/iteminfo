package jp.arith.minecraft.iteminfo.tooltip;

import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

public class DefenceTooltip extends Tooltip {
    public DefenceTooltip() { super("defence"); }

    @Override
    public String getDefaultColor() { return "GOLD"; }

    @Override
    public String getLine(ItemStack itemStack) {
        Item item = itemStack.getItem();
        if(item instanceof ItemArmor) {
            Item material = ((ItemArmor) item).getArmorMaterial().func_151685_b();
            String armorTypeName = "iteminfo.armortype." + ((ItemArmor) item).armorType;

            if(StatCollector.canTranslate(armorTypeName))
                armorTypeName = StatCollector.translateToLocal(armorTypeName);
            else
                armorTypeName = "" + ((ItemArmor) item).armorType;

            String damageReduceAmount = "+" + ((ItemArmor) item).damageReduceAmount + " " + StatCollector.translateToLocal("iteminfo.tooltip.defence");

            if (material != null)
                return damageReduceAmount + " (" + armorTypeName + "; " + new ItemStack(material, 1).getDisplayName() + ")";
            else
                return damageReduceAmount + " (" + armorTypeName + ")";
        }

        return "";
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.VISIBLE; }
}
