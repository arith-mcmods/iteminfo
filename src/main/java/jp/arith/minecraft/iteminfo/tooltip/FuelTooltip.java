package jp.arith.minecraft.iteminfo.tooltip;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.StatCollector;

public class FuelTooltip extends Tooltip {
    public FuelTooltip() { super("fuel"); }

    @Override
    public String getDefaultColor() { return "RED"; }

    @Override
    public String getLine(ItemStack itemStack) {


        int fuelValue = TileEntityFurnace.getItemBurnTime(itemStack);
        if(fuelValue > 0)
            return StatCollector.translateToLocalFormatted("iteminfo.tooltip.fuel", fuelValue)
                   + " (" + StatCollector.translateToLocalFormatted("iteminfo.tooltip.fuel.items", fuelValue / 200.0) + ")";
        return "";
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.VISIBLE; }
}
