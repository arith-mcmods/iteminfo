package jp.arith.minecraft.iteminfo.tooltip;

public enum Tooltips {
    DURABILITY(new DurabilityTooltip()),
    DEFENCE(new DefenceTooltip()),
    FUEL(new FuelTooltip()),
    ITEM_REGISTERED_NAME(new ItemRegisteredNameTooltip()),
    ITEM_UNLOCALIZED_NAME(new ItemUnlocalizedNameTooltip()),
    ITEM_CLASS_NAME(new ItemClassNameTooltip()),
    BLOCK_HARVEST_LEVEL(new BlockHarvestLevelTooltip()),
    ITEM_HARVEST_LEVEL(new ItemHarvestLevelTooltip()),
    ORE_DICTIONARY(new OreDictionaryTooltip()),
    ;

    public final Tooltip tooltip;
    Tooltips(Tooltip tooltip) {
        this.tooltip = tooltip;
    }
}
