package jp.arith.minecraft.iteminfo.tooltip;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemClassNameTooltip extends Tooltip {
    public ItemClassNameTooltip() { super("itemClassName"); }

    @Override
    public String getDefaultColor() { return "DARK_GRAY"; }

    @Override
    public String getLine(ItemStack itemStack) {
        if(itemStack.getItem() instanceof ItemBlock)
            return ((ItemBlock) itemStack.getItem()).field_150939_a.getClass().getName();
        else
            return itemStack.getItem().getClass().getName();
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.ADVANCED; }
}
