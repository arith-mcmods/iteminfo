package jp.arith.minecraft.iteminfo.tooltip;

import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

public class DurabilityTooltip extends Tooltip {
    public DurabilityTooltip() { super("durability"); }

    @Override
    public String getDefaultColor() { return "GREEN"; }

    @Override
    public String getLine(ItemStack itemStack) {
        int maxDamage = itemStack.getMaxDamage();
        if(maxDamage != 0)
            return StatCollector.translateToLocalFormatted("iteminfo.tooltip.durability", (maxDamage - itemStack.getItemDamage()), maxDamage);
        return "";
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.VISIBLE; }
}
