package jp.arith.minecraft.iteminfo.tooltip;

import com.mojang.realmsclient.gui.ChatFormatting;
import jp.arith.minecraft.iteminfo.Config;
import jp.arith.minecraft.iteminfo.ItemInfoMod;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ItemHarvestLevelTooltip extends Tooltip {
    public ItemHarvestLevelTooltip() { super("itemHarvestLevel"); }

    @Override
    public String getDefaultColor() { return "BLUE"; }

    @Override
    public String getLine(ItemStack itemStack) {
        Item item = itemStack.getItem();
        List<String> list = new ArrayList<String>();

        String specialColor = ItemInfoMod.getConfig().tooltip.color("itemHarvestLevelWithGetToolClasses", "GREEN");
        String baseColor = ItemInfoMod.getConfig().tooltip.color(name, "BLUE");

        for(String clz : ItemInfoMod.getConfig().getToolList()) {
            int level = item.getHarvestLevel(itemStack, clz);
            if(level >= 0)
                list.add(baseColor + clz + "(" + level + ")");
        }

        for(String clz : item.getToolClasses(itemStack)) {
            int level = item.getHarvestLevel(itemStack, clz);
            int index = list.indexOf(baseColor + clz + "(" + level + ")");
            if(index < 0)
                list.add(specialColor + clz + "(" + level + ")");
            else
                list.set(index, specialColor + clz + "(" + level + ")");
        }

        return joinStrings(ChatFormatting.DARK_GRAY + ", ", list);
    }

    @Override
    public void reloadConfig(Config config) {
        config.tooltip.color("itemHarvestLevelWithGetToolClasses", "GREEN");
        super.reloadConfig(config);
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.ADVANCED; }
}
