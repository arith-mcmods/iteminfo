package jp.arith.minecraft.iteminfo.tooltip;

public enum Visibility {
    VISIBLE(true, true), ADVANCED(false, true), NOTADVANCED(true, false), HIDDEN(false, false);

    public final boolean isVisibleInNotAdvanced, isVisibleInAdvanced;

    Visibility(boolean visibleInNotAdvanced, boolean visibleInAdvanced) {
        this.isVisibleInNotAdvanced = visibleInNotAdvanced;
        this.isVisibleInAdvanced = visibleInAdvanced;
    }

    public String getConfigValue() { return this.name().toLowerCase(); }

    public static Visibility fromConfigValue(String val) { return Visibility.valueOf(val.toUpperCase()); }
}
