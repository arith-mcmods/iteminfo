package jp.arith.minecraft.iteminfo.tooltip;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemRegisteredNameTooltip extends Tooltip {
    public ItemRegisteredNameTooltip() { super("itemRegisteredName"); }

    @Override
    public String getDefaultColor() { return "DARK_AQUA"; }

    @Override
    public String getLine(ItemStack itemStack) {
        return Item.itemRegistry.getNameForObject(itemStack.getItem());
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.ADVANCED; }
}
