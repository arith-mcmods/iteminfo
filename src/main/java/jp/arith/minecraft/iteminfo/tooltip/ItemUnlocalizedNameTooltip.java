package jp.arith.minecraft.iteminfo.tooltip;

import net.minecraft.item.ItemStack;

public class ItemUnlocalizedNameTooltip extends Tooltip {
    public ItemUnlocalizedNameTooltip() { super("itemUnlocalizedName"); }

    @Override
    public String getDefaultColor() { return "DARK_PURPLE"; }

    @Override
    public String getLine(ItemStack itemStack) {
        return itemStack.getUnlocalizedName();
    }

    @Override
    public Visibility getDefaultVisibility() { return Visibility.ADVANCED; }
}
