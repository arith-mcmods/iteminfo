package jp.arith.minecraft.iteminfo;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import jp.arith.minecraft.iteminfo.tooltip.Tooltips;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;

public class ClientProxy extends CommonProxy {

    @Override
    public void init() {
        MinecraftForge.EVENT_BUS.register(this);
        FMLCommonHandler.instance().bus().register(this);
    }

    @SubscribeEvent
    public void itemTooltipEvent(ItemTooltipEvent e) {
        for(Tooltips tt : Tooltips.values())
            tt.tooltip.appendTooltip(e);
    }

    @SubscribeEvent
    public void livingUpdate(LivingEvent.LivingUpdateEvent e)
    {
        if(e.entity instanceof EntityPlayer && notifyConfigReloaded) {
            ((EntityPlayer) e.entity).addChatComponentMessage(new ChatComponentText("BreakAll: Reloaded config."));

            notifyConfigReloaded = false;
        }
    }

    private boolean notifyConfigReloaded;

    @SubscribeEvent
    public void tick(TickEvent.ClientTickEvent e) {
        notifyConfigReloaded = notifyConfigReloaded || ItemInfoMod.getConfig().pollUpdate();
    }
}
