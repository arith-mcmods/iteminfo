package jp.arith.minecraft.iteminfo;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkCheckHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

@Mod(modid = ItemInfoMod.MODID,name = ItemInfoMod.NAME, version = ItemInfoMod.VERSION)
public class ItemInfoMod {
    public static final String MODID = "arith.iteminfo";
    public static final String VERSION = "0.0.2";
    public static final String NAME = "iteminfo";

    public static final Logger LOG = LogManager.getLogger("iteminfo");

    @SidedProxy(
            clientSide = "jp.arith.minecraft.iteminfo.ClientProxy",
            serverSide = "jp.arith.minecraft.iteminfo.CommonProxy")
    public static CommonProxy proxy;

    @EventHandler
    public void init(FMLInitializationEvent e) {
        proxy.init();
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent e) { config = new Config(e.getSuggestedConfigurationFile()); }

    static Config config;
    public static Config getConfig() { return config; }

    @NetworkCheckHandler
    public boolean networkCheck(Map<String, String> mods, Side side) { return true; }

    public static boolean isPlayerOpped(EntityPlayer player) {
        return MinecraftServer.getServer().getConfigurationManager().func_152596_g(player.getGameProfile());
    }
}
